# Rental Application
Coding task for SIXT.
[Link](https://www.figma.com/file/G2RhIhbpYSCkjEjbLPIgG7/Sixt-Test-Task?node-id=0%3A1) to app screens in figma.

## Libraries used

### [Moxy](https://github.com/moxy-community/Moxy)
Used for Model View Presenter pattern implementation with out of box orientation handling functionality

### [RxJava2](https://github.com/ReactiveX/RxJava)
Mainly used for asynchronous work

### [Dagger 2](https://github.com/google/dagger) 
Used for dependency injection

