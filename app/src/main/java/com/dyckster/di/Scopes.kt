package com.dyckster.di

import javax.inject.Scope

@Scope
annotation class PerFragment

@Scope
annotation class PerActivity

@Scope
annotation class PerApplication
