package com.dyckster.di.modules

import android.content.Context
import android.net.ConnectivityManager
import com.dyckster.RentalApp
import com.dyckster.di.PerApplication
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: RentalApp): Context = application

    @Provides
    @PerApplication
    fun provideConnectivityManager(context: Context): ConnectivityManager {
        return context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

}