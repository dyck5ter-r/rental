package com.dyckster.di.modules

import android.net.ConnectivityManager
import com.dyckster.BuildConfig
import com.dyckster.data.network.NetworkConnectionInterceptor
import com.dyckster.data.network.RentalApi
import com.dyckster.di.PerApplication
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    @PerApplication
    fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    @Provides
    @PerApplication
    fun provideOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        networkConnectionInterceptor: NetworkConnectionInterceptor
    ): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(httpLoggingInterceptor)
        httpClient.addInterceptor(networkConnectionInterceptor)
        return httpClient.build()
    }

    @Provides
    @PerApplication
    fun provideNetworkConnectionInterceptor(connectivityManager: ConnectivityManager) =
        NetworkConnectionInterceptor(connectivityManager)

    @Provides
    @PerApplication
    fun provideAppApi(httpClient: OkHttpClient): RentalApi {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(httpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .build()
            .create(RentalApi::class.java)
    }

}