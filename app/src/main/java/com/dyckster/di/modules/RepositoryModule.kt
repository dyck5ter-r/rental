package com.dyckster.di.modules

import com.dyckster.data.mapper.CarMapper
import com.dyckster.data.network.RentalApi
import com.dyckster.data.repository.AppCarRepository
import com.dyckster.di.PerApplication
import com.dyckster.domain.repository.CarRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    @PerApplication
    fun provideCarsRepository(rentalApi: RentalApi, carMapper: CarMapper): CarRepository =
        AppCarRepository(rentalApi, carMapper)

}