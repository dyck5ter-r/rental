package com.dyckster.di.modules

import com.dyckster.di.PerActivity
import com.dyckster.di.PerFragment
import com.dyckster.presentation.rental.RentalActivity
import com.dyckster.presentation.rentalinfo.RentalInfoFragment
import com.dyckster.presentation.rentallist.RentalListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface AppBuilderModule {

    @PerActivity
    @ContributesAndroidInjector
    fun provideMainActivity(): RentalActivity

    @PerFragment
    @ContributesAndroidInjector
    fun provideRentalInfoFragment(): RentalInfoFragment

    @PerFragment
    @ContributesAndroidInjector
    fun provideRentalListFragment(): RentalListFragment

}