package com.dyckster.di

import com.dyckster.RentalApp
import com.dyckster.di.modules.AppBuilderModule
import com.dyckster.di.modules.ApplicationModule
import com.dyckster.di.modules.NetworkModule
import com.dyckster.di.modules.RepositoryModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@PerApplication
@Component(
    modules = [AndroidSupportInjectionModule::class,
        AppBuilderModule::class,
        ApplicationModule::class,
        RepositoryModule::class,
        NetworkModule::class]
)
interface AppComponent : AndroidInjector<RentalApp> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<RentalApp>()

}