package com.dyckster.domain.model

enum class FuelType(private val fieldValue: String = "") {
    ELECTRIC("E"),
    DIESEL("D"),
    PETROL("P"),
    UNKNOWN;

    companion object {
        fun fromString(value: String) =
            values().find { it.fieldValue.equals(value, true) } ?: UNKNOWN
    }
}