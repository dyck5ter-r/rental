package com.dyckster.domain.model

enum class CleanlinessLevel {
    CLEAN,
    REGULAR,
    VERY_CLEAN,
    UNKNOWN;

    companion object {
        fun fromString(value: String) = values().find { it.name.equals(value, true) } ?: UNKNOWN
    }
}