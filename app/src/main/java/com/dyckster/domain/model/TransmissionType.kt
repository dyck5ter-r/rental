package com.dyckster.domain.model

enum class TransmissionType(private val fieldValue: String = "") {
    AUTOMATIC("A"),
    MECHANICAL("M"),
    UNKNOWN;

    companion object {
        fun fromString(value: String) =
            values().find { it.fieldValue.equals(value, true) } ?: UNKNOWN
    }
}