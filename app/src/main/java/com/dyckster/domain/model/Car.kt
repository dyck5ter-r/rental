package com.dyckster.domain.model

data class Car(
    val id: String,
    val modelId: String,
    val modelName: String,
    val name: String,
    val manufacturer: String,
    val group: String,
    val color: String,
    val series: String,
    val fuelType: FuelType,
    val fuelLevel: Float,
    val transmissionType: TransmissionType,
    val licensePlate: String,
    val latitude: Double,
    val longitude: Double,
    val innerCleanliness: CleanlinessLevel,
    val carImageUrl: String?
)