package com.dyckster.domain.repository

import com.dyckster.domain.model.Car
import io.reactivex.Single

interface CarRepository {

    fun getCars(forced: Boolean): Single<List<Car>>

    fun getCarInfo(id: String): Single<Car>
}