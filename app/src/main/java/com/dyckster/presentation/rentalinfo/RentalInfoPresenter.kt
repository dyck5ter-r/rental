package com.dyckster.presentation.rentalinfo

import com.dyckster.domain.repository.CarRepository
import com.dyckster.presentation.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class RentalInfoPresenter @Inject constructor(private val carRepository: CarRepository) :
    BasePresenter<RentalInfoView>() {

    fun fetchInfo(rentalId: String) {
        carRepository.getCarInfo(rentalId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.showRentalInfo(it)
            }, {
                viewState.onFailedToOpenRentalInfo()
            })
            .also { disposable.add(it) }
    }

}