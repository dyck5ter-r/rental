package com.dyckster.presentation.rentalinfo

import com.dyckster.domain.model.Car
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

interface RentalInfoView : MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showRentalInfo(car: Car)

    @StateStrategyType(SkipStrategy::class)
    fun onFailedToOpenRentalInfo()

}