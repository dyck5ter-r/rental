package com.dyckster.presentation.rentalinfo

import android.os.Bundle
import android.view.View
import com.dyckster.R
import com.dyckster.domain.model.Car
import com.dyckster.domain.model.CleanlinessLevel
import com.dyckster.domain.model.FuelType
import com.dyckster.domain.model.TransmissionType
import com.dyckster.presentation.base.BaseFragment
import com.dyckster.util.delegate.argument
import com.dyckster.util.ext.changeVisibility
import com.dyckster.util.ext.fuelIcon
import com.dyckster.util.ext.fuelLevelPercent
import com.dyckster.util.ext.transmissionIcon
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_rental_info.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject
import javax.inject.Provider

private const val ARGUMENT_RENTAL_ID = "rental_id"

class RentalInfoFragment : BaseFragment(), RentalInfoView {

    @Inject
    lateinit var provider: Provider<RentalInfoPresenter>

    @InjectPresenter
    lateinit var presenter: RentalInfoPresenter

    @ProvidePresenter
    fun providePresenter(): RentalInfoPresenter = provider.get()

    private val rentalId: String by argument(ARGUMENT_RENTAL_ID)

    override fun layoutRes() = R.layout.fragment_rental_info

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.fetchInfo(rentalId)
    }

    override fun showRentalInfo(car: Car) {
        //Load image with alpha animation
        Picasso.get().load(car.carImageUrl)
            .error(R.drawable.img_car_placeholder)
            .noFade()
            .into(carImage, object : Callback {
                override fun onSuccess() {
                    carImage.alpha = 0f
                    carImage.animate().alpha(1f).setDuration(350L).start()
                }

                override fun onError(e: Exception) = Unit
            })
        carName.text = getString(R.string.car_name_format, car.modelName, car.licensePlate)
        fuelLevelIcon.setImageResource(car.fuelIcon())
        fuelLevelText.text = getString(
            if (car.fuelType == FuelType.ELECTRIC) R.string.fuel_level_electric
            else R.string.fuel_level_liquid, car.fuelLevelPercent()
        )
        transmissionIcon.setImageResource(car.transmissionIcon())
        transmissionText.text = getString(
            R.string.transmission_type, getString(
                if (car.transmissionType == TransmissionType.AUTOMATIC)
                    R.string.transmission_auto else R.string.transmission_mechanical
            )
        )
        cleanlinessGroup.changeVisibility(car.innerCleanliness != CleanlinessLevel.REGULAR)
        cleanlinessText.text = getString(
            when (car.innerCleanliness) {
                CleanlinessLevel.CLEAN -> R.string.clean_level_clean
                CleanlinessLevel.VERY_CLEAN,
                CleanlinessLevel.REGULAR,
                CleanlinessLevel.UNKNOWN -> R.string.clean_level_very_clean
            }
        )
    }

    override fun onFailedToOpenRentalInfo() {
        activity?.onBackPressed()
    }

    companion object {

        fun newInstance(rentalId: String): RentalInfoFragment {
            val fragment = RentalInfoFragment()
            val args = Bundle()
            args.putString(ARGUMENT_RENTAL_ID, rentalId)
            fragment.arguments = args
            return fragment
        }

    }
}