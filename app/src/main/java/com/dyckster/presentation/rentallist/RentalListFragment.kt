package com.dyckster.presentation.rentallist

import android.content.Context
import android.os.Bundle
import android.view.View
import com.dyckster.R
import com.dyckster.domain.model.Car
import com.dyckster.presentation.base.BaseFragment
import com.dyckster.presentation.rental.RentalListListener
import com.dyckster.presentation.rentallist.adapter.RentalAdapter
import com.dyckster.util.ext.changeVisibility
import com.dyckster.util.ext.onClick
import com.dyckster.util.ext.toast
import kotlinx.android.synthetic.main.fragment_rental_list.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject
import javax.inject.Provider

class RentalListFragment : BaseFragment(), RentalListView {

    @Inject
    lateinit var provider: Provider<RentalListPresenter>

    @InjectPresenter
    lateinit var presenter: RentalListPresenter

    @ProvidePresenter
    fun providePresenter(): RentalListPresenter = provider.get()

    private var rentalListListener: RentalListListener? = null
    private val carAdapter = RentalAdapter { presenter.onCarClick(it) }

    override fun layoutRes() = R.layout.fragment_rental_list

    override fun onAttach(context: Context) {
        super.onAttach(context)
        rentalListListener = context as? RentalListListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rentalList.adapter = carAdapter
        tryAgainButton.onClick { presenter.fetchCars() }
    }

    override fun showCars(cars: List<Car>) {
        carAdapter.cars = cars
        rentalList.scheduleLayoutAnimation()
    }

    override fun openCarInfo(car: Car) {
        rentalListListener?.openRentalInfoFromList(car)
    }

    override fun showNoConnectionError(show: Boolean) {
        noConnectionGroup.changeVisibility(show)
    }

    override fun showGenericError() {
        context?.toast(R.string.error_generic)
    }

    override fun onDetach() {
        super.onDetach()
        rentalListListener = null
    }
}