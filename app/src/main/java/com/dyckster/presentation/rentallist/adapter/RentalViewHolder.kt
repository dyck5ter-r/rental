package com.dyckster.presentation.rentallist.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dyckster.R
import com.dyckster.domain.model.Car
import com.dyckster.domain.model.TransmissionType
import com.dyckster.util.ext.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_rental_info.view.*

class RentalViewHolder(
    parent: ViewGroup,
    private val listener: (Car) -> Unit
) : RecyclerView.ViewHolder(parent.inflate(R.layout.item_rental_info)) {

    fun bind(car: Car) {
        with(itemView) {
            Picasso.get().load(car.carImageUrl).error(R.drawable.img_car_placeholder).into(carImage)
            carName.text = car.modelName
            transmissionGroup.changeVisibility(car.transmissionType != TransmissionType.UNKNOWN)
            transmissionText.setText(
                if (car.transmissionType == TransmissionType.AUTOMATIC) {
                    R.string.transmission_auto
                } else {
                    R.string.transmission_mechanical
                }
            )
            fuelLevelText.text =
                context.getString(R.string.fuel_level_template, car.fuelLevelPercent())
            fuelLevelIcon.setImageResource(car.fuelIcon())
            transmissionIcon.setImageResource(car.transmissionIcon())

            onClick { listener.invoke(car) }
        }
    }

}