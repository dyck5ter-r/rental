package com.dyckster.presentation.rentallist

import com.dyckster.domain.model.Car
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

interface RentalListView : MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showCars(cars: List<Car>)

    @StateStrategyType(SkipStrategy::class)
    fun openCarInfo(car: Car)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showNoConnectionError(show: Boolean)

    @StateStrategyType(SkipStrategy::class)
    fun showGenericError()

}