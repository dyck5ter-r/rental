package com.dyckster.presentation.rentallist

import com.dyckster.data.exception.NoConnectionException
import com.dyckster.domain.model.Car
import com.dyckster.domain.repository.CarRepository
import com.dyckster.presentation.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class RentalListPresenter @Inject constructor(private val carRepository: CarRepository) :
    BasePresenter<RentalListView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        fetchCars()
    }

    fun fetchCars() {
        carRepository.getCars(false)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { viewState.showNoConnectionError(false) }
            .subscribe({
                viewState.showCars(it)
            }, {
                if (it is NoConnectionException) {
                    viewState.showNoConnectionError(true)
                } else {
                    viewState.showGenericError()
                }
            })
            .also { disposable.add(it) }
    }

    fun onCarClick(car: Car) = viewState.openCarInfo(car)

}