package com.dyckster.presentation.rentallist.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dyckster.domain.model.Car

class RentalAdapter(private val listener: (Car) -> Unit) :
    RecyclerView.Adapter<RentalViewHolder>() {

    var cars: List<Car> = emptyList()
        set(value) {
            val oldList = field
            field = value
            DiffUtil.calculateDiff(Diff(oldList, value)).dispatchUpdatesTo(this)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        RentalViewHolder(parent, listener)

    override fun getItemCount() = cars.size

    override fun onBindViewHolder(holder: RentalViewHolder, position: Int) {
        holder.bind(cars[position])
    }

    private class Diff(private val oldCars: List<Car>, private val newCars: List<Car>) :
        DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldCars[oldItemPosition].id == newCars[newItemPosition].id
        }

        override fun getOldListSize() = oldCars.size

        override fun getNewListSize() = newCars.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            areItemsTheSame(oldItemPosition, newItemPosition)
    }
}