package com.dyckster.presentation.base

import android.os.Bundle
import dagger.android.AndroidInjection
import io.reactivex.disposables.CompositeDisposable
import moxy.MvpAppCompatActivity

abstract class BaseActivity : MvpAppCompatActivity() {

    protected val uiDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        uiDisposable.clear()
    }

}