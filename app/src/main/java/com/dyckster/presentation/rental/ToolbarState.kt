package com.dyckster.presentation.rental

import com.dyckster.R

sealed class ToolbarState {
    abstract fun getTitle(): Int
}

object ListToolbarState : ToolbarState() {
    override fun getTitle() = R.string.title_available_cars
}

object MapToolbarState : ToolbarState() {
    override fun getTitle() = R.string.app_name
}