package com.dyckster.presentation.rental

import com.dyckster.data.exception.NoConnectionException
import com.dyckster.data.exception.RentalInfoNotFoundException
import com.dyckster.domain.model.Car
import com.dyckster.domain.repository.CarRepository
import com.dyckster.presentation.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class RentalPresenter @Inject constructor(private val carRepository: CarRepository) :
    BasePresenter<RentalView>() {

    private val carsSubject = BehaviorSubject.create<List<Car>>()
    private var selectedCar: Car? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.switchToolbarState(MapToolbarState)
        fetchCars()
    }

    fun fetchCars() {
        carRepository.getCars(true)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { viewState.showLoader(true) }
            .doAfterTerminate { viewState.showLoader(false) }
            .doOnSuccess { viewState.showNoNetworkException(false) }
            .subscribe({
                carsSubject.onNext(it)
            }, {
                if (it is NoConnectionException) {
                    viewState.showNoNetworkException(true)
                } else {
                    viewState.showGenericError()
                }
            })
            .also { disposable.add(it) }
    }


    fun openRentalInfo(rentalId: String) {
        carRepository.getCarInfo(rentalId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.openRentalInfo(it, selectedCar == null)
                selectedCar = it
            }, {
                when (it) {
                    is RentalInfoNotFoundException -> viewState.showRentalInfoNotFoundError()
                    is NoConnectionException -> viewState.showNoNetworkException(true)
                    else -> viewState.showGenericError()
                }
            })
            .also { disposable.add(it) }
    }

    fun onBack() {
        if (selectedCar != null) {
            viewState.closeRentalInfo(selectedCar?.id)
            selectedCar = null
        } else {
            viewState.back()
        }
    }

    fun switchBackState(entryCount: Int) =
        viewState.switchToolbarState(if (entryCount > 0) ListToolbarState else MapToolbarState)

    fun onMapReady() = initCarsSubject()
    fun openRentalList() = viewState.openRentalList()

    /**
     * Init cars subject is used to wait for map initialization finish.
     */
    private fun initCarsSubject() {
        carsSubject.observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                viewState.showCars(it, selectedCar?.id)
                selectedCar?.let { c -> viewState.openRentalInfo(c, true) }
            }
            .also { disposable.add(it) }
    }

}