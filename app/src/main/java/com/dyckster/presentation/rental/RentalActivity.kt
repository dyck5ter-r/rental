package com.dyckster.presentation.rental

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE
import com.dyckster.R
import com.dyckster.domain.model.Car
import com.dyckster.presentation.base.BaseActivity
import com.dyckster.presentation.rentalinfo.RentalInfoFragment
import com.dyckster.presentation.rentallist.RentalListFragment
import com.dyckster.util.delegate.MapAnimationDelegate
import com.dyckster.util.delegate.MarkerBuilderDelegate
import com.dyckster.util.ext.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_rental.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject
import javax.inject.Provider

private const val DETAILED_ZOOM_LEVEL = 13f
private const val EXTRA_CAM_POSITION = "camera_pos"

class RentalActivity : BaseActivity(), RentalView, RentalListListener, OnMapReadyCallback,
    FragmentManager.OnBackStackChangedListener {

    @Inject
    lateinit var mapAnimDelegate: MapAnimationDelegate
    @Inject
    lateinit var markerBuilderDelegate: MarkerBuilderDelegate

    @Inject
    lateinit var provider: Provider<RentalPresenter>

    @InjectPresenter
    lateinit var presenter: RentalPresenter

    @ProvidePresenter
    fun providePresenter(): RentalPresenter = provider.get()

    private var map: GoogleMap? = null
    private var markers: Map<String, Marker?> = HashMap()
    private val defaultMarker by lazy { markerBuilderDelegate.getDefaultMarker(R.dimen.default_marker_size) }
    private val bigMarker by lazy { markerBuilderDelegate.getDefaultMarker(R.dimen.selected_marker_size) }

    private val mapBoundsPadding: Int by lazy { resources.getDimensionPixelSize(R.dimen.map_bounds_padding) }
    private val mapHeight: Int by lazy { mapContainer.height }

    private var savedCameraPosition: CameraPosition? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rental)
        if (savedInstanceState != null) {
            savedCameraPosition =
                savedInstanceState.getParcelable(EXTRA_CAM_POSITION) as? CameraPosition
        }
        (supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment)?.getMapAsync(this)

        listButton.onClick { presenter.openRentalList() }
        backButton.onClick { supportFragmentManager.popBackStack() }
        noConnectionTryAgain.onClick { presenter.fetchCars() }
    }

    override fun onStart() {
        super.onStart()
        supportFragmentManager.addOnBackStackChangedListener(this)
    }

    override fun onMapReady(map: GoogleMap) {
        this.map = map
        savedCameraPosition?.also {
            map.moveCamera(CameraUpdateFactory.newCameraPosition(it))
        }

        map.setOnMarkerClickListener {
            presenter.openRentalInfo(it.snippet)
            return@setOnMarkerClickListener true
        }
        presenter.onMapReady()
    }

    override fun showCars(cars: List<Car>, selectedRental: String?) {
        noConnectionGroup.hide()
        val bound = LatLngBounds.builder()
        markers = cars
            .onEach { bound.include(it.latLng()) }
            .associate { car ->
                val selectedCar = selectedRental == car.id
                val markerOptions = MarkerOptions()
                    .position(car.latLng())
                    .snippet(car.id)
                    .icon(if (selectedCar) bigMarker else defaultMarker)

                car.id to map?.addMarker(markerOptions)
            }

        if (savedCameraPosition == null) {
            map?.moveCamera(CameraUpdateFactory.newLatLngBounds(bound.build(), mapBoundsPadding))
        } else {
            map?.moveCamera(CameraUpdateFactory.newCameraPosition(savedCameraPosition))
        }
    }


    override fun openRentalInfoFromList(car: Car) {
        supportFragmentManager.popBackStack()
        presenter.openRentalInfo(car.id)
    }

    override fun openRentalInfo(car: Car, openWithAnimation: Boolean) {
        markers.values.onEach { it?.setIcon(if (it.id == car.id) bigMarker else defaultMarker) }
        markers[car.id]?.setIcon(bigMarker)

        val zoom =
            map?.cameraPosition?.zoom?.takeIf { it > DETAILED_ZOOM_LEVEL } ?: DETAILED_ZOOM_LEVEL

        map?.animateCamera(CameraUpdateFactory.newLatLngZoom(car.latLng(), zoom))
        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .replace(R.id.rentalInfoFrame, RentalInfoFragment.newInstance(car.id))
            .runOnCommit {
                if (openWithAnimation) {
                    waitForLayoutAndAnimateMap()
                }
            }
            .commit()
    }

    override fun closeRentalInfo(rentalId: String?) {
        markers[rentalId]?.setIcon(defaultMarker)
        mapAnimDelegate.mapAnimator(false, mapContainer, mapHeight, rentalInfoFrame.height)
            .start()
        with(supportFragmentManager) {
            findFragmentById(R.id.rentalInfoFrame)?.also {
                beginTransaction().remove(it).commit()
            }
        }
    }


    override fun openRentalList() {
        map?.stopAnimation()
        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .replace(R.id.rentalListFrame, RentalListFragment())
            .setTransition(TRANSIT_FRAGMENT_FADE)
            .addToBackStack(null)
            .commit()
    }

    override fun switchToolbarState(toolbarState: ToolbarState) {
        listButton.changeVisibility(toolbarState is MapToolbarState)
        backButton.changeVisibility(toolbarState !is MapToolbarState)
        titleText.setText(toolbarState.getTitle())
    }


    private fun waitForLayoutAndAnimateMap() {
        rentalInfoFrame.afterMeasured {
            if (height > 0) {
                mapAnimDelegate.mapAnimator(true, mapContainer, mapHeight, height).start()
            }
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            presenter.onBack()
        }
    }

    override fun onStop() {
        super.onStop()
        supportFragmentManager.removeOnBackStackChangedListener(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(EXTRA_CAM_POSITION, map?.cameraPosition)
    }

    override fun back() = super.onBackPressed()

    override fun onBackStackChanged() =
        presenter.switchBackState(supportFragmentManager.backStackEntryCount)

    override fun showRentalInfoNotFoundError() = toast(R.string.error_no_rental_info)

    override fun showNoNetworkException(show: Boolean) = noConnectionGroup.changeVisibility(show)

    override fun showGenericError() = toast(R.string.error_generic)

    override fun showLoader(show: Boolean) = progressBar.changeVisibility(show)


}
