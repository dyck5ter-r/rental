package com.dyckster.presentation.rental

import com.dyckster.domain.model.Car

interface RentalListListener {

    fun openRentalInfoFromList(car: Car)

}