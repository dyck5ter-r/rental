package com.dyckster.presentation.rental

import com.dyckster.domain.model.Car
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

interface RentalView : MvpView {

    @StateStrategyType(SkipStrategy::class)
    fun showCars(cars: List<Car>, selectedRental: String?)

    @StateStrategyType(SkipStrategy::class)
    fun openRentalInfo(car: Car, openWithAnimation: Boolean)

    @StateStrategyType(SkipStrategy::class)
    fun back()

    @StateStrategyType(SkipStrategy::class)
    fun closeRentalInfo(rentalId: String?)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun switchToolbarState(toolbarState: ToolbarState)

    @StateStrategyType(SkipStrategy::class)
    fun openRentalList()

    @StateStrategyType(SkipStrategy::class)
    fun showRentalInfoNotFoundError()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showNoNetworkException(show: Boolean)

    @StateStrategyType(SkipStrategy::class)
    fun showGenericError()

    @StateStrategyType(SkipStrategy::class)
    fun showLoader(show: Boolean)

}