package com.dyckster.util

abstract class BaseMapper<in T, out K> {

    abstract fun transform(obj: T): K

    fun transform(list: List<T>): List<K> {
        return list.asSequence()
            .map { transform(it) }
            .toList()
    }

}