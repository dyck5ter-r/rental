package com.dyckster.util.ext

import com.dyckster.R
import com.dyckster.domain.model.Car
import com.dyckster.domain.model.FuelType
import com.dyckster.domain.model.TransmissionType
import com.google.android.gms.maps.model.LatLng

fun Car.latLng() = LatLng(latitude, longitude)

fun Car.transmissionIcon(): Int = when (transmissionType) {
    TransmissionType.AUTOMATIC -> R.drawable.ic_transmission_auto_24dp
    TransmissionType.MECHANICAL -> R.drawable.ic_transmission_mechanical_24dp
    TransmissionType.UNKNOWN -> R.drawable.ic_transmission_mechanical_24dp
}

fun Car.fuelIcon(): Int = when (fuelType) {
    FuelType.ELECTRIC -> R.drawable.ic_fuel_electro_24dp
    FuelType.DIESEL -> R.drawable.ic_fuel_diesel_24dp
    FuelType.PETROL -> R.drawable.ic_fuel_petrol_24dp
    FuelType.UNKNOWN -> R.drawable.ic_fuel_unknown_24dp
}

fun Car.fuelLevelPercent(): Int = (fuelLevel * 100).toInt()
