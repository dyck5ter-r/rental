package com.dyckster.util.ext

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.view.View
import android.widget.Toast
import androidx.annotation.DimenRes
import androidx.annotation.StringRes
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager


fun Activity.getScreenHeight(): Int {
    val display = this.windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    return size.y
}

fun Activity.getScreenWidth(): Int {
    val display = this.windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    return size.x
}

fun Context.getDimensionInt(@DimenRes dimen: Int) = resources.getDimensionPixelOffset(dimen)

fun View.getDimensionInt(@DimenRes dimen: Int) = context.resources.getDimensionPixelOffset(dimen)

fun View.getString(@StringRes res: Int) = context.getString(res)

fun View.getString(@StringRes res: Int, vararg formats: Any) = context.getString(res, *formats)

fun DialogFragment.show(fm: FragmentManager) = show(fm, javaClass.simpleName)

fun Context.toast(text: CharSequence) = Toast.makeText(this, text, Toast.LENGTH_LONG).show()

fun Context.toast(res: Int) = toast(getText(res))
