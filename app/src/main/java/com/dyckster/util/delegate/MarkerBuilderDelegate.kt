package com.dyckster.util.delegate

import android.content.Context
import android.graphics.*
import androidx.annotation.DimenRes
import com.dyckster.R
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import javax.inject.Inject


class MarkerBuilderDelegate @Inject constructor(private val context: Context) {

    fun getDefaultMarker(@DimenRes size: Int): BitmapDescriptor {
        val sizeScaled = context.resources.getDimensionPixelSize(size)
        val markerBitmap =
            BitmapFactory.decodeResource(context.resources, R.drawable.img_car_marker, null)
        return BitmapDescriptorFactory.fromBitmap(scaleBitmap(markerBitmap, sizeScaled, sizeScaled))
    }

    private fun scaleBitmap(bitmap: Bitmap, newWidth: Int, newHeight: Int): Bitmap? {
        val scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888)
        val scaleX = newWidth / bitmap.width.toFloat()
        val scaleY = newHeight / bitmap.height.toFloat()
        val pivotX = 0f
        val pivotY = 0f
        val scaleMatrix = Matrix()
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY)
        val canvas = Canvas(scaledBitmap)
        canvas.setMatrix(scaleMatrix)
        canvas.drawBitmap(bitmap, 0f, 0f, Paint(Paint.FILTER_BITMAP_FLAG))
        return scaledBitmap
    }

}