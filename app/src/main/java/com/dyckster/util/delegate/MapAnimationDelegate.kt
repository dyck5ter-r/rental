package com.dyckster.util.delegate

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.RelativeLayout
import androidx.core.view.updateLayoutParams
import com.dyckster.R
import javax.inject.Inject

private const val MAP_ANIMATION_DURATION = 300L

class MapAnimationDelegate @Inject constructor(private val context: Context) {

    private val mapMarginValue by lazy { context.resources.getDimensionPixelSize(R.dimen.map_side_margin) }
    private val bottomViewImageHeight by lazy { context.resources.getDimensionPixelSize(R.dimen.rental_info_image_height) }
    private val bottomOverlapPercent by lazy {
        val value = TypedValue()
        context.resources.getValue(R.dimen.rental_info_overlap_percent, value, true)
        return@lazy value.float
    }

    fun mapAnimator(
        animateIn: Boolean,
        mapContainer: View,
        defaultMapHeight: Int,
        bottomTranslationHeight: Int
    ): Animator {
        val set = AnimatorSet()
        val heightValue =
            (defaultMapHeight - bottomTranslationHeight + (bottomViewImageHeight * bottomOverlapPercent)).toInt()

        val animator = ValueAnimator.ofInt(
            if (animateIn) 0 else mapMarginValue,
            if (animateIn) mapMarginValue else 0
        )
        animator.duration = MAP_ANIMATION_DURATION
        animator.addUpdateListener {
            mapContainer.updateLayoutParams<RelativeLayout.LayoutParams> {
                val value = it.animatedValue as Int
                setMargins(value, 0, value, 0)
            }
        }
        val heightAnimator = ValueAnimator.ofInt(
            if (animateIn) defaultMapHeight else heightValue,
            if (animateIn) heightValue else defaultMapHeight
        )
        heightAnimator.addUpdateListener {
            mapContainer.updateLayoutParams<RelativeLayout.LayoutParams> {
                height = it.animatedValue as Int
            }
        }

        set.interpolator = AccelerateDecelerateInterpolator()
        set.playTogether(animator, heightAnimator)

        return set
    }

}