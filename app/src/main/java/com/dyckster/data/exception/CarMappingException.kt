package com.dyckster.data.exception

class CarMappingException : Throwable(message = "Failed to map car")