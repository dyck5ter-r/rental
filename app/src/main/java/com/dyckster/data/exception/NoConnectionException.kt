package com.dyckster.data.exception

class NoConnectionException : Throwable(message = "Internet connection not available")