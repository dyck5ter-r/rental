package com.dyckster.data.exception

class RentalInfoNotFoundException : Throwable()