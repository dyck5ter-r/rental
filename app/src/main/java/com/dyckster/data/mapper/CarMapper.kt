package com.dyckster.data.mapper

import com.dyckster.data.exception.CarMappingException
import com.dyckster.data.model.CarModel
import com.dyckster.domain.model.Car
import com.dyckster.domain.model.CleanlinessLevel
import com.dyckster.domain.model.FuelType
import com.dyckster.domain.model.TransmissionType
import com.dyckster.util.BaseMapper
import javax.inject.Inject

class CarMapper @Inject constructor() : BaseMapper<CarModel, Car>() {

    override fun transform(obj: CarModel): Car {
        if (obj.id == null) throw CarMappingException()
        return Car(
            id = obj.id,
            modelId = obj.modelIdentifier,
            modelName = obj.modelName,
            name = obj.name,
            manufacturer = obj.make,
            group = obj.group,
            color = obj.color,
            series = obj.series,
            fuelType = FuelType.fromString(obj.fuelType),
            fuelLevel = obj.fuelLevel,
            transmissionType = TransmissionType.fromString(obj.transmission),
            licensePlate = obj.licensePlate,
            latitude = obj.latitude,
            longitude = obj.longitude,
            innerCleanliness = CleanlinessLevel.fromString(obj.innerCleanliness),
            carImageUrl = obj.imageUrl
        )
    }

}