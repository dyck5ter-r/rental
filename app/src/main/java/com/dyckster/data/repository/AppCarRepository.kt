package com.dyckster.data.repository

import com.dyckster.data.exception.RentalInfoNotFoundException
import com.dyckster.data.mapper.CarMapper
import com.dyckster.data.network.RentalApi
import com.dyckster.domain.model.Car
import com.dyckster.domain.repository.CarRepository
import io.reactivex.Single
import javax.inject.Inject

class AppCarRepository @Inject constructor(
    private val rentalApi: RentalApi,
    private val carMapper: CarMapper
) : CarRepository {

    private var cachedCars: List<Car> = emptyList()

    override fun getCars(forced: Boolean): Single<List<Car>> {
        if (!forced && cachedCars.isNotEmpty()) return Single.just(cachedCars)
        return rentalApi.requestCars()
            .map { carMapper.transform(it) }
            .doOnSuccess { cachedCars = it }
    }

    override fun getCarInfo(id: String): Single<Car> {
        return Single.just(cachedCars.find { it.id == id } ?: throw RentalInfoNotFoundException())
    }

}