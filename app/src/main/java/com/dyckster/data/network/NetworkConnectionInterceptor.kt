package com.dyckster.data.network

import android.net.ConnectivityManager
import com.dyckster.data.exception.NoConnectionException
import okhttp3.Interceptor
import okhttp3.Response

class NetworkConnectionInterceptor(private val connectivityManager: ConnectivityManager) :
    Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isConnected()) {
            throw NoConnectionException()
        }
        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }

    private fun isConnected(): Boolean {
        return connectivityManager.activeNetworkInfo?.isConnected == true
    }

}