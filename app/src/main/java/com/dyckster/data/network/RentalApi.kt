package com.dyckster.data.network

import com.dyckster.data.model.CarModel
import io.reactivex.Single
import retrofit2.http.GET

interface RentalApi {

    @GET("cars")
    fun requestCars(): Single<List<CarModel>>

}