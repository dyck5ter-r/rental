package com.dyckster.presentation.rental

import com.dyckster.data.exception.NoConnectionException
import com.dyckster.data.exception.RentalInfoNotFoundException
import com.dyckster.domain.repository.CarRepository
import com.dyckster.util.CarFactory
import com.dyckster.util.ImmediateScheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyList
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class RentalPresenterTest {

    @Mock
    lateinit var carRepository: CarRepository

    @Mock
    lateinit var rentalInfoViewState: `RentalView$$State`

    private lateinit var presenter: RentalPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = RentalPresenter(carRepository)
        presenter.setViewState(rentalInfoViewState)

        RxJavaPlugins.setIoSchedulerHandler { ImmediateScheduler() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { ImmediateScheduler() }
    }

    @Test
    fun `attach and fetch before map`() {
        val repo = mock(CarRepository::class.java)
        val presenter = RentalPresenter(repo)
        val cars = CarFactory.carList()
        `when`(repo.getCars(true)).thenReturn(Single.just(cars))

        presenter.setViewState(rentalInfoViewState)
        presenter.attachView(mock(RentalView::class.java))

        verify(rentalInfoViewState).attachView(any())

        verify(rentalInfoViewState).switchToolbarState(MapToolbarState)
        verify(rentalInfoViewState).showLoader(true)
        verify(rentalInfoViewState).showLoader(false)
        verify(rentalInfoViewState).showNoNetworkException(false)
        verify(rentalInfoViewState, never()).showCars(cars, null)

        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `attach and fetch and map init`() {
        val repo = mock(CarRepository::class.java)
        val presenter = RentalPresenter(repo)
        val cars = CarFactory.carList()
        `when`(repo.getCars(true)).thenReturn(Single.just(cars))

        presenter.setViewState(rentalInfoViewState)
        presenter.attachView(mock(RentalView::class.java))

        verify(rentalInfoViewState).attachView(any())

        verify(rentalInfoViewState).switchToolbarState(MapToolbarState)
        verify(rentalInfoViewState).showLoader(true)
        verify(rentalInfoViewState).showLoader(false)
        verify(rentalInfoViewState).showNoNetworkException(false)
        verify(rentalInfoViewState, never()).showCars(cars, null)

        presenter.onMapReady()

        verify(rentalInfoViewState).showCars(cars, null)

        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `fetch cars before map init`() {
        `when`(carRepository.getCars(true)).thenReturn(Single.just(anyList()))

        presenter.fetchCars()
        verify(rentalInfoViewState).showLoader(true)
        verify(rentalInfoViewState).showLoader(false)
        verify(rentalInfoViewState).showNoNetworkException(false)

        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `fetch cars with connection error`() {
        `when`(carRepository.getCars(true)).thenReturn(Single.error(NoConnectionException()))
        presenter.onMapReady()

        presenter.fetchCars()
        verify(rentalInfoViewState).showLoader(true)
        verify(rentalInfoViewState).showLoader(false)
        verify(rentalInfoViewState, never()).showCars(anyList(), any())
        verify(rentalInfoViewState, never()).showGenericError()
        verify(rentalInfoViewState).showNoNetworkException(true)

        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `fetch cars with any error`() {
        `when`(carRepository.getCars(true)).thenReturn(Single.error(Throwable()))
        presenter.onMapReady()

        presenter.fetchCars()
        verify(rentalInfoViewState).showLoader(true)
        verify(rentalInfoViewState).showLoader(false)
        verify(rentalInfoViewState, never()).showCars(anyList(), any())
        verify(rentalInfoViewState, never()).showNoNetworkException(false)
        verify(rentalInfoViewState).showGenericError()

        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `fetch cars and map init`() {
        `when`(carRepository.getCars(true)).thenReturn(Single.just(anyList()))

        presenter.fetchCars()
        verify(rentalInfoViewState).showLoader(true)
        verify(rentalInfoViewState).showLoader(false)
        verify(rentalInfoViewState).showNoNetworkException(false)
        verify(rentalInfoViewState, never()).showCars(anyList(), any())

        presenter.onMapReady()

        verify(rentalInfoViewState).showCars(anyList(), any())

        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `map ready before fetch cars`() {
        val cars = CarFactory.carList()
        `when`(carRepository.getCars(true)).thenReturn(Single.just(cars))

        presenter.onMapReady()
        verify(rentalInfoViewState, never()).showCars(cars, null)

        presenter.fetchCars()
        verify(rentalInfoViewState).showLoader(true)
        verify(rentalInfoViewState).showLoader(false)
        verify(rentalInfoViewState).showNoNetworkException(false)

        verify(rentalInfoViewState).showCars(cars, null)

        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `open rental info first time`() {
        val car = CarFactory.singleCar("test")
        `when`(carRepository.getCarInfo(car.id)).thenReturn(Single.just(car))

        presenter.openRentalInfo(car.id)

        verify(rentalInfoViewState).openRentalInfo(car, true)
        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `open rental info twice`() {
        val car1 = CarFactory.singleCar("test1")
        val car2 = CarFactory.singleCar("test2")
        `when`(carRepository.getCarInfo(car1.id))
            .thenReturn(Single.just(car1))
        `when`(carRepository.getCarInfo(car2.id))
            .thenReturn(Single.just(car2))

        presenter.openRentalInfo(car1.id)
        verify(rentalInfoViewState).openRentalInfo(car1, true)
        presenter.openRentalInfo(car2.id)
        //Ensure second time opened without animation
        verify(rentalInfoViewState).openRentalInfo(car2, false)

        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `open rental info and show cars again`() {
        val cars = CarFactory.carList()
        val car = CarFactory.singleCar("test")
        `when`(carRepository.getCars(true)).thenReturn(Single.just(cars))
        `when`(carRepository.getCarInfo(car.id)).thenReturn(Single.just(car))

        presenter.fetchCars()
        presenter.onMapReady()
        verify(rentalInfoViewState).showCars(cars, null)

        presenter.openRentalInfo(car.id)

        verify(rentalInfoViewState).openRentalInfo(car, true)

        presenter.fetchCars()
        verify(rentalInfoViewState, never()).closeRentalInfo(car.id)
        verify(rentalInfoViewState).showCars(cars, car.id)
        verify(rentalInfoViewState, times(2)).openRentalInfo(car, true)
    }

    @Test
    fun `open rental info and go back`() {
        val car = CarFactory.singleCar("test")
        `when`(carRepository.getCarInfo(car.id)).thenReturn(Single.just(car))

        presenter.openRentalInfo(car.id)
        presenter.onBack()
        presenter.openRentalInfo(car.id)

        verify(rentalInfoViewState, times(2)).openRentalInfo(car, true)
        verify(rentalInfoViewState).closeRentalInfo(car.id)

        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `open rental info twice and go back`() {
        val car = CarFactory.singleCar("test")
        `when`(carRepository.getCarInfo(car.id)).thenReturn(Single.just(car))

        presenter.openRentalInfo(car.id)
        presenter.openRentalInfo(car.id)
        presenter.onBack()
        presenter.openRentalInfo(car.id)

        verify(rentalInfoViewState, times(2)).openRentalInfo(car, true)
        verify(rentalInfoViewState).openRentalInfo(car, false)
        verify(rentalInfoViewState).closeRentalInfo(car.id)
        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `open rental info but rental not found`() {
        val car = CarFactory.singleCar("test")
        `when`(carRepository.getCarInfo(car.id)).thenReturn(Single.error(RentalInfoNotFoundException()))

        presenter.openRentalInfo(car.id)

        verify(rentalInfoViewState).showRentalInfoNotFoundError()
        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `open rental info but internet disconnected`() {
        val car = CarFactory.singleCar("test")
        `when`(carRepository.getCarInfo(car.id)).thenReturn(Single.error(NoConnectionException()))

        presenter.openRentalInfo(car.id)

        verify(rentalInfoViewState).showNoNetworkException(true)
        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `open rental info with generic error`() {
        val car = CarFactory.singleCar("test")
        `when`(carRepository.getCarInfo(car.id)).thenReturn(Single.error(Throwable()))

        presenter.openRentalInfo(car.id)

        verify(rentalInfoViewState).showGenericError()
        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `show cars and go back`() {
        val cars = CarFactory.carList()
        val car = CarFactory.singleCar("test")
        `when`(carRepository.getCars(true)).thenReturn(Single.just(cars))
        `when`(carRepository.getCarInfo(car.id)).thenReturn(Single.just(car))

        presenter.fetchCars()
        presenter.onMapReady()
        presenter.onBack()

        verify(rentalInfoViewState).showLoader(true)
        verify(rentalInfoViewState).showLoader(false)
        verify(rentalInfoViewState).showNoNetworkException(false)
        verify(rentalInfoViewState).showCars(cars, null)
        verify(rentalInfoViewState).back()
        verify(rentalInfoViewState, never()).closeRentalInfo(any())
        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `open rental list`() {
        presenter.openRentalList()
        verify(rentalInfoViewState).openRentalList()
        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `switch back state`() {
        presenter.switchBackState(0)
        presenter.switchBackState(1)
        presenter.switchBackState(200)

        verify(rentalInfoViewState).switchToolbarState(MapToolbarState)
        verify(rentalInfoViewState, times(2)).switchToolbarState(ListToolbarState)
        verifyNoMoreInteractions(rentalInfoViewState)
    }


}