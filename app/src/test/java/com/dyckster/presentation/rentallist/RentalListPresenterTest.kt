package com.dyckster.presentation.rentallist

import com.dyckster.data.exception.NoConnectionException
import com.dyckster.domain.repository.CarRepository
import com.dyckster.util.CarFactory
import com.dyckster.util.ImmediateScheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class RentalListPresenterTest {

    @Mock
    lateinit var carRepository: CarRepository

    @Mock
    lateinit var rentalListViewState: `RentalListView$$State`

    private lateinit var presenter: RentalListPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = RentalListPresenter(carRepository)
        presenter.setViewState(rentalListViewState)

        RxJavaPlugins.setIoSchedulerHandler { ImmediateScheduler() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { ImmediateScheduler() }
    }

    @Test
    fun `attach and fetch`() {
        val repo = mock(CarRepository::class.java)
        val presenter = RentalListPresenter(repo)
        `when`(repo.getCars(false)).thenReturn(Single.just(emptyList()))

        presenter.setViewState(rentalListViewState)
        presenter.attachView(mock(RentalListView::class.java))

        verify(rentalListViewState).showCars(ArgumentMatchers.any())
        verify(rentalListViewState, never()).showGenericError()
    }

    @Test
    fun `load cars`() {
        val cars = CarFactory.carList()
        `when`(carRepository.getCars(false)).thenReturn(Single.just(cars))

        presenter.fetchCars()

        verify(rentalListViewState).showCars(cars)
        verify(rentalListViewState, never()).showGenericError()
        verify(rentalListViewState).showNoConnectionError(false)
        verify(rentalListViewState, never()).showNoConnectionError(true)
        verifyNoMoreInteractions(rentalListViewState)
    }


    @Test
    fun `load cars with no internet`() {
        val cars = CarFactory.carList()
        `when`(carRepository.getCars(false)).thenReturn(Single.error(NoConnectionException()))

        presenter.fetchCars()

        verify(rentalListViewState, never()).showCars(cars)
        verify(rentalListViewState, never()).showGenericError()
        verify(rentalListViewState).showNoConnectionError(true)
        verifyNoMoreInteractions(rentalListViewState)
    }

    @Test
    fun `load cars with generic error`() {
        val cars = CarFactory.carList()
        `when`(carRepository.getCars(false)).thenReturn(Single.error(Throwable()))

        presenter.fetchCars()

        verify(rentalListViewState, never()).showCars(cars)
        verify(rentalListViewState).showGenericError()
        verify(rentalListViewState, never()).showNoConnectionError(ArgumentMatchers.anyBoolean())
        verifyNoMoreInteractions(rentalListViewState)
    }

    @Test
    fun `open car`() {
        val car = CarFactory.singleCar("carId")

        presenter.onCarClick(car)

        verify(rentalListViewState).openCarInfo(car)
        verifyZeroInteractions(carRepository)
        verifyNoMoreInteractions(rentalListViewState)
    }


}