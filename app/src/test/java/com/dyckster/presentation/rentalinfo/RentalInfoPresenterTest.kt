package com.dyckster.presentation.rentalinfo

import com.dyckster.domain.repository.CarRepository
import com.dyckster.util.CarFactory
import com.dyckster.util.ImmediateScheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations


class RentalInfoPresenterTest {

    @Mock
    lateinit var carRepository: CarRepository

    @Mock
    lateinit var rentalInfoViewState: `RentalInfoView$$State`

    private lateinit var presenter: RentalInfoPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = RentalInfoPresenter(carRepository)
        presenter.setViewState(rentalInfoViewState)

        RxJavaPlugins.setIoSchedulerHandler { ImmediateScheduler() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { ImmediateScheduler() }
    }

    @Test
    fun `fetch rental info success`() {
        val rentalId = "id"
        val car = CarFactory.singleCar(rentalId)
        `when`(carRepository.getCarInfo(rentalId)).thenReturn(Single.just(car))
        presenter.fetchInfo(rentalId)

        verify(rentalInfoViewState).showRentalInfo(car)
        verify(rentalInfoViewState, never()).onFailedToOpenRentalInfo()
        verifyNoMoreInteractions(rentalInfoViewState)
    }

    @Test
    fun `fetch rental info error`() {
        val rentalId = "id"
        val car = CarFactory.singleCar(rentalId)
        `when`(carRepository.getCarInfo(rentalId)).thenReturn(Single.error(Throwable()))
        presenter.fetchInfo(rentalId)

        verify(rentalInfoViewState).onFailedToOpenRentalInfo()
        verify(rentalInfoViewState, never()).showRentalInfo(car)
        verifyNoMoreInteractions(rentalInfoViewState)
    }

}