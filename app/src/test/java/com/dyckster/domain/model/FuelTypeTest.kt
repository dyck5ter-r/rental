package com.dyckster.domain.model

import junit.framework.Assert
import org.junit.Test

class FuelTypeTest {

    @Test
    fun `validate inputs`() {
        //7th symbol is a russian letter "р"
        val inputs = listOf("E", "D", "P", "e", "d", "p", "р", "Diesel", "Hello world")
        val expectedResults = listOf(
            FuelType.ELECTRIC,
            FuelType.DIESEL,
            FuelType.PETROL,
            FuelType.ELECTRIC,
            FuelType.DIESEL,
            FuelType.PETROL,
            FuelType.UNKNOWN,
            FuelType.UNKNOWN,
            FuelType.UNKNOWN
        )
        val results = inputs.map { FuelType.fromString(it) }
        Assert.assertEquals(expectedResults, results)
    }

}