package com.dyckster.domain.model

import junit.framework.Assert.assertEquals
import org.junit.Test

class TransmissionTypeTest {

    @Test
    fun `validate inputs`() {
        //5th "a" is a russian letter "a"
        val inputs = listOf("A", "M", "a", "m", "а", "Hello world!", "Automatic")
        val expectedResults = listOf(
            TransmissionType.AUTOMATIC,
            TransmissionType.MECHANICAL,
            TransmissionType.AUTOMATIC,
            TransmissionType.MECHANICAL,
            TransmissionType.UNKNOWN,
            TransmissionType.UNKNOWN,
            TransmissionType.UNKNOWN
        )
        val results = inputs.map { TransmissionType.fromString(it) }
        assertEquals(expectedResults, results)
    }

}