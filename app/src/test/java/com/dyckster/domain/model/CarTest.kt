package com.dyckster.domain.model

import com.dyckster.util.CarFactory
import org.junit.Assert
import org.junit.Test

class CarTest {

    @Test
    fun `equality test`() {
        val firstCar = CarFactory.singleCar("1")
        val secondCar = CarFactory.singleCar("1")
        val thirdCar = CarFactory.singleCar("3")

        Assert.assertEquals(firstCar, secondCar)
        Assert.assertNotSame(firstCar, secondCar)
        Assert.assertFalse(secondCar == thirdCar)
        Assert.assertFalse(secondCar == thirdCar)
    }
}