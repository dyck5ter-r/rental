package com.dyckster.domain.model

import junit.framework.Assert
import org.junit.Test

class CleanlinessLevelTest {

    @Test
    fun `validate inputs`() {
        val inputs = listOf(
            "CLEAN",
            "clean",
            "ClEaN",
            "Very_Clean",
            "very_clean",
            "VERY_CLEAN",
            "very clean",
            "REGULAR",
            "regular",
            "reGuLaR",
            "Hello world"
        )
        val expectedResults = listOf(
            CleanlinessLevel.CLEAN,
            CleanlinessLevel.CLEAN,
            CleanlinessLevel.CLEAN,
            CleanlinessLevel.VERY_CLEAN,
            CleanlinessLevel.VERY_CLEAN,
            CleanlinessLevel.VERY_CLEAN,
            CleanlinessLevel.UNKNOWN,
            CleanlinessLevel.REGULAR,
            CleanlinessLevel.REGULAR,
            CleanlinessLevel.REGULAR,
            CleanlinessLevel.UNKNOWN
        )
        val results = inputs.map { CleanlinessLevel.fromString(it) }
        Assert.assertEquals(expectedResults, results)
    }

}