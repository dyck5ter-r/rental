package com.dyckster.util

import com.dyckster.data.model.CarModel
import com.dyckster.domain.model.Car
import com.dyckster.domain.model.CleanlinessLevel
import com.dyckster.domain.model.FuelType
import com.dyckster.domain.model.TransmissionType

object CarFactory {

    fun singleCar(
        carId: String,
        transmissionType: TransmissionType = TransmissionType.UNKNOWN,
        fuelType: FuelType = FuelType.UNKNOWN
    ): Car {
        return Car(
            id = carId,
            modelId = "Model ID",
            modelName = "Model Name",
            name = "Name",
            manufacturer = "Manufacturer",
            group = "Group",
            color = "Green",
            series = "Series",
            fuelType = fuelType,
            fuelLevel = 0.5f,
            transmissionType = transmissionType,
            licensePlate = "M-311111",
            latitude = 0.0,
            longitude = 0.0,
            innerCleanliness = CleanlinessLevel.UNKNOWN,
            carImageUrl = null
        )
    }

    fun singleCarModel(carId: String): CarModel {
        return CarModel(
            id = carId,
            modelIdentifier = "Model ID",
            modelName = "Model Name",
            name = "Name",
            make = "Manufacturer",
            group = "Group",
            color = "Green",
            series = "Series",
            fuelType = "P",
            fuelLevel = 0.5f,
            transmission = "M",
            licensePlate = "M-311111",
            latitude = 0.0,
            longitude = 0.0,
            innerCleanliness = "CLEAN",
            imageUrl = "car.png"
        )
    }

    fun carList(carCount: Int = 10): List<Car> {
        val cars = ArrayList<Car>()
        for (i in 0..carCount) {
            cars.add(singleCar(i.toString()))
        }
        return cars
    }


    fun carModelList(carCount: Int = 10): ArrayList<CarModel> {
        val cars = ArrayList<CarModel>()
        for (i in 0..carCount) {
            cars.add(singleCarModel(i.toString()))
        }
        return cars
    }
}