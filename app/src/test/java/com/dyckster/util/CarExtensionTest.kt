package com.dyckster.util

import com.dyckster.R
import com.dyckster.domain.model.FuelType
import com.dyckster.domain.model.TransmissionType
import com.dyckster.util.ext.fuelIcon
import com.dyckster.util.ext.fuelLevelPercent
import com.dyckster.util.ext.latLng
import com.dyckster.util.ext.transmissionIcon
import com.google.android.gms.maps.model.LatLng
import junit.framework.Assert.assertEquals
import org.junit.Test

class CarExtensionTest {

    @Test
    fun `transmission drawable`() {
        val carUnknown =
            CarFactory.singleCar(carId = "tr", transmissionType = TransmissionType.UNKNOWN)
        val carAuto =
            CarFactory.singleCar(carId = "tr", transmissionType = TransmissionType.AUTOMATIC)
        val carMechanical =
            CarFactory.singleCar(carId = "tr", transmissionType = TransmissionType.MECHANICAL)

        assert(carUnknown.transmissionType == TransmissionType.UNKNOWN)
        assertEquals(R.drawable.ic_transmission_mechanical_24dp, carUnknown.transmissionIcon())
        assert(carMechanical.transmissionType == TransmissionType.MECHANICAL)
        assertEquals(R.drawable.ic_transmission_mechanical_24dp, carMechanical.transmissionIcon())
        assert(carAuto.transmissionType == TransmissionType.AUTOMATIC)
        assertEquals(R.drawable.ic_transmission_auto_24dp, carAuto.transmissionIcon())
    }

    @Test
    fun `fuel drawable`() {
        val carUnknown = CarFactory.singleCar("fuel")
        val carPetrol = CarFactory.singleCar(carId = "fuel", fuelType = FuelType.PETROL)
        val carElectric = CarFactory.singleCar(carId = "fuel", fuelType = FuelType.ELECTRIC)
        val carDiesel = CarFactory.singleCar(carId = "fuel", fuelType = FuelType.DIESEL)

        assert(carPetrol.fuelType == FuelType.PETROL)
        assertEquals(R.drawable.ic_fuel_petrol_24dp, carPetrol.fuelIcon())
        assert(carElectric.fuelType == FuelType.ELECTRIC)
        assertEquals(R.drawable.ic_fuel_electro_24dp, carElectric.fuelIcon())
        assert(carDiesel.fuelType == FuelType.DIESEL)
        assertEquals(R.drawable.ic_fuel_diesel_24dp, carDiesel.fuelIcon())
        assert(carUnknown.fuelType == FuelType.UNKNOWN)
        assertEquals(R.drawable.ic_fuel_unknown_24dp, carUnknown.fuelIcon())
    }

    @Test
    fun `latlng extension`() {
        val car = CarFactory.singleCar("latlng")
        assert(car.latitude == 0.0 && car.longitude == 0.0)

        val latLng = car.latLng()
        val expectedLatLng = LatLng(0.0, 0.0)
        assertEquals(expectedLatLng, latLng)
    }

    @Test
    fun `fuel percent`() {
        val car = CarFactory.singleCar("id")
        val fuelPercent = car.fuelLevelPercent()
        assertEquals(50, fuelPercent)
    }

}