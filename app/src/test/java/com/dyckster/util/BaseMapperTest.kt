package com.dyckster.util

import junit.framework.Assert.assertEquals
import org.junit.Test

class BaseMapperTest {

    private val sampleMapper = object : BaseMapper<String, Int>() {
        override fun transform(obj: String) = obj.toInt()
    }

    @Test
    fun transform() {
        val res = sampleMapper.transform("1")
        assertEquals(1, res)
    }

    @Test
    fun transformList() {
        val res = sampleMapper.transform(listOf("1", "2", "3"))
        assertEquals(listOf(1, 2, 3), res)
    }

    @Test(expected = Throwable::class)
    fun transformWithError() {
        sampleMapper.transform("Hello world")
    }

    @Test(expected = Throwable::class)
    fun transformListWithError() {
        sampleMapper.transform(listOf("1", "2.2", "3"))
    }
}