package com.dyckster.data.network

import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.dyckster.data.exception.NoConnectionException
import junit.framework.Assert.assertEquals
import okhttp3.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

class NetworkConnectionInterceptorTest {

    @Mock
    lateinit var connectivityManager: ConnectivityManager
    @Mock
    lateinit var networkInfo: NetworkInfo

    private lateinit var connectionInterceptor: NetworkConnectionInterceptor

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        connectionInterceptor = NetworkConnectionInterceptor(connectivityManager)
    }

    @Test
    fun `has connection`() {
        val chain = mock(Interceptor.Chain::class.java)
        val mockRequest = Request.Builder()
            .url("https://google.com")
            .build()
        val mockResponse = mockResponse(mockRequest)
        `when`(networkInfo.isConnected).thenReturn(true)
        `when`(connectivityManager.activeNetworkInfo).thenReturn(networkInfo)
        `when`(chain.request()).thenReturn(mockRequest)
        `when`(chain.proceed(ArgumentMatchers.any())).thenReturn(mockResponse)

        val resp = connectionInterceptor.intercept(chain)

        assertEquals(mockResponse, resp)
    }

    @Test(expected = NoConnectionException::class)
    fun `no connection`() {
        val chain = mock(Interceptor.Chain::class.java)
        `when`(networkInfo.isConnected).thenReturn(false)
        `when`(connectivityManager.activeNetworkInfo).thenReturn(networkInfo)
        connectionInterceptor.intercept(chain)
    }


    private fun mockResponse(request: Request): Response {
        return Response.Builder()
            .request(request)
            .protocol(Protocol.HTTP_2)
            .code(200) // status code
            .message("")
            .body(
                ResponseBody.create(MediaType.get("application/json; charset=utf-8"), "{}")
            )
            .build()
    }

}