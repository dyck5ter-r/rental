package com.dyckster.data.repository

import com.dyckster.data.exception.RentalInfoNotFoundException
import com.dyckster.data.mapper.CarMapper
import com.dyckster.data.network.RentalApi
import com.dyckster.domain.repository.CarRepository
import com.dyckster.util.CarFactory
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class AppCarRepositoryTest {

    @Mock
    lateinit var rentalApi: RentalApi

    private val mapper = CarMapper()

    private lateinit var repository: CarRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        repository = AppCarRepository(rentalApi, mapper)
    }

    @Test
    fun getCars() {
        val initlist = CarFactory.carModelList()
        val mappedValues = mapper.transform(initlist)
        `when`(rentalApi.requestCars()).thenReturn(Single.just(initlist))
        val cars = repository.getCars(true).blockingGet()
        assertEquals(mappedValues, cars)
    }

    @Test
    fun getCarsCached() {
        val initlist = CarFactory.carModelList()
        val listAfter = CarFactory.carModelList(20)

        `when`(rentalApi.requestCars()).thenReturn(Single.just(initlist))
        val cars = repository.getCars(true).blockingGet()
        `when`(rentalApi.requestCars()).thenReturn(Single.just(listAfter))
        val cachedCars = repository.getCars(false).blockingGet()

        assertEquals(cars, cachedCars)
        assertNotEquals(cachedCars, listAfter)
    }

    @Test
    fun getCarInfo() {
        val initlist = CarFactory.carModelList()
        val firstId = "0"
        `when`(rentalApi.requestCars()).thenReturn(Single.just(initlist))
        val cars = repository.getCars(true).blockingGet()
        val car = repository.getCarInfo(firstId).blockingGet()

        assertEquals(cars.first(), car)
        assertEquals(cars.first().id, initlist.first().id)
        assertEquals(initlist.first().id, car.id)
    }

    @Test(expected = RentalInfoNotFoundException::class)
    fun getNonExistingRental() {
        val id = "-1"
        val initlist = CarFactory.carModelList()
        `when`(rentalApi.requestCars()).thenReturn(Single.just(initlist))
        repository.getCars(true).blockingGet()
        repository.getCarInfo(id).blockingGet()
    }
}