package com.dyckster.data.mapper

import com.dyckster.data.exception.CarMappingException
import com.dyckster.data.model.CarModel
import com.dyckster.domain.model.CleanlinessLevel
import com.dyckster.domain.model.FuelType
import com.dyckster.domain.model.TransmissionType
import com.google.gson.Gson
import junit.framework.Assert.assertEquals
import org.junit.Test

class CarMapperTest {

    @Test
    fun `map valid car model`() {
        val carModel = Gson().fromJson(rawCar(), CarModel::class.java)
        val car = CarMapper().transform(carModel)
        assertEquals(carModel.id, car.id)
        assertEquals(carModel.modelIdentifier, car.modelId)
        assertEquals(carModel.modelName, car.modelName)
        assertEquals(carModel.name, car.name)
        assertEquals(carModel.group, car.group)
        assertEquals(carModel.color, car.color)
        assertEquals(carModel.series, car.series)
        assertEquals(carModel.fuelLevel, car.fuelLevel)
        assertEquals(carModel.licensePlate, car.licensePlate)

        assertEquals(FuelType.DIESEL, car.fuelType)
        assertEquals(TransmissionType.MECHANICAL, car.transmissionType)
        assertEquals(CleanlinessLevel.CLEAN, car.innerCleanliness)

        assertEquals(carModel.latitude, car.latitude)
        assertEquals(carModel.longitude, car.longitude)

        assertEquals(carModel.imageUrl, car.carImageUrl)
    }


    @Test(expected = CarMappingException::class)
    fun `map invalid car model`() {
        val carModel = Gson().fromJson(carWithoutId(), CarModel::class.java)
        CarMapper().transform(carModel)
    }

    @Test
    fun `map valid car but unknown enum types`() {
        val carModel = Gson().fromJson(rawCarUnknownEnumTypes(), CarModel::class.java)
        val car = CarMapper().transform(carModel)
        assertEquals(carModel.id, car.id)
        assertEquals(carModel.modelIdentifier, car.modelId)
        assertEquals(carModel.modelName, car.modelName)
        assertEquals(carModel.name, car.name)
        assertEquals(carModel.group, car.group)
        assertEquals(carModel.make,car.manufacturer)
        assertEquals(carModel.color, car.color)
        assertEquals(carModel.series, car.series)
        assertEquals(carModel.fuelLevel, car.fuelLevel)
        assertEquals(carModel.licensePlate, car.licensePlate)

        assertEquals(FuelType.UNKNOWN, car.fuelType)
        assertEquals(TransmissionType.UNKNOWN, car.transmissionType)
        assertEquals(CleanlinessLevel.UNKNOWN, car.innerCleanliness)

        assertEquals(carModel.latitude, car.latitude)
        assertEquals(carModel.longitude, car.longitude)

        assertEquals(carModel.imageUrl, car.carImageUrl)
    }

    fun rawCar(): String {
        return "{\n" +
                "\"id\": \"WMWSW31060T222495\",\n" +
                "\"modelIdentifier\": \"mini\",\n" +
                "\"modelName\": \"MINI\",\n" +
                "\"name\": \"Angie\",\n" +
                "\"make\": \"BMW\",\n" +
                "\"group\": \"MINI\",\n" +
                "\"color\": \"midnight_black\",\n" +
                "\"series\": \"MINI\",\n" +
                "\"fuelType\": \"D\",\n" +
                "\"fuelLevel\": 0.4,\n" +
                "\"transmission\": \"M\",\n" +
                "\"licensePlate\": \"M-VO0244\",\n" +
                "\"latitude\": 48.152207,\n" +
                "\"longitude\": 11.572649,\n" +
                "\"innerCleanliness\": \"CLEAN\",\n" +
                "\"carImageUrl\": \"https://cdn.sixt.io/codingtask/images/mini.png\"\n" +
                "}"
    }

    fun rawCarUnknownEnumTypes(): String {
        return "{\n" +
                "\"id\": \"WMWSW31060T222495\",\n" +
                "\"modelIdentifier\": \"mini\",\n" +
                "\"modelName\": \"MINI\",\n" +
                "\"name\": \"Angie\",\n" +
                "\"make\": \"BMW\",\n" +
                "\"group\": \"MINI\",\n" +
                "\"color\": \"midnight_black\",\n" +
                "\"series\": \"MINI\",\n" +
                "\"fuelType\": \"O\",\n" +
                "\"fuelLevel\": 0.4,\n" +
                "\"transmission\": \"V\",\n" +
                "\"licensePlate\": \"M-VO0244\",\n" +
                "\"latitude\": 48.152207,\n" +
                "\"longitude\": 11.572649,\n" +
                "\"innerCleanliness\": \"WHAT\",\n" +
                "\"carImageUrl\": \"https://cdn.sixt.io/codingtask/images/mini.png\"\n" +
                "}"
    }

    fun carWithoutId(): String {
        return "{\n" +
                "\"modelIdentifier\": \"mini\",\n" +
                "\"modelName\": \"MINI\",\n" +
                "\"name\": \"Angie\",\n" +
                "\"make\": \"BMW\",\n" +
                "\"group\": \"MINI\",\n" +
                "\"color\": \"midnight_black\",\n" +
                "\"series\": \"MINI\",\n" +
                "\"fuelType\": \"D\",\n" +
                "\"fuelLevel\": 0.4,\n" +
                "\"transmission\": \"M\",\n" +
                "\"licensePlate\": \"M-VO0244\",\n" +
                "\"latitude\": 48.152207,\n" +
                "\"longitude\": 11.572649,\n" +
                "\"innerCleanliness\": \"CLEAN\",\n" +
                "\"carImageUrl\": \"https://cdn.sixt.io/codingtask/images/mini.png\"\n" +
                "}"
    }

}