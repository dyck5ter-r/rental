package com.dyckster.data.model

import com.google.gson.Gson
import junit.framework.Assert.assertEquals
import org.junit.Test

class CarModelTest {

    @Test
    fun `parse valid json`() {
        val car = Gson().fromJson(rawCar(), CarModel::class.java)
        assertEquals("WMWSW31060T222495", car.id)
        assertEquals(0.4f, car.fuelLevel)
    }

    @Test
    fun `parse invalid json`() {
        //Gson throws no exception when null is passed from model. Consider moving to Moshi
        val invalidCar = Gson().fromJson(carWithoutId(), CarModel::class.java)
        assertEquals(null, invalidCar.id)
    }

    fun rawCar(): String {
        return "{\n" +
                "\"id\": \"WMWSW31060T222495\",\n" +
                "\"modelIdentifier\": \"mini\",\n" +
                "\"modelName\": \"MINI\",\n" +
                "\"name\": \"Angie\",\n" +
                "\"make\": \"BMW\",\n" +
                "\"group\": \"MINI\",\n" +
                "\"color\": \"midnight_black\",\n" +
                "\"series\": \"MINI\",\n" +
                "\"fuelType\": \"D\",\n" +
                "\"fuelLevel\": 0.4,\n" +
                "\"transmission\": \"M\",\n" +
                "\"licensePlate\": \"M-VO0244\",\n" +
                "\"latitude\": 48.152207,\n" +
                "\"longitude\": 11.572649,\n" +
                "\"innerCleanliness\": \"CLEAN\",\n" +
                "\"carImageUrl\": \"https://cdn.sixt.io/codingtask/images/mini.png\"\n" +
                "}"
    }

    fun carWithoutId(): String {
        return "{\n" +
                "\"modelIdentifier\": \"mini\",\n" +
                "\"modelName\": \"MINI\",\n" +
                "\"name\": \"Angie\",\n" +
                "\"make\": \"BMW\",\n" +
                "\"group\": \"MINI\",\n" +
                "\"color\": \"midnight_black\",\n" +
                "\"series\": \"MINI\",\n" +
                "\"fuelType\": \"D\",\n" +
                "\"fuelLevel\": 0.4,\n" +
                "\"transmission\": \"M\",\n" +
                "\"licensePlate\": \"M-VO0244\",\n" +
                "\"latitude\": 48.152207,\n" +
                "\"longitude\": 11.572649,\n" +
                "\"innerCleanliness\": \"CLEAN\",\n" +
                "\"carImageUrl\": \"https://cdn.sixt.io/codingtask/images/mini.png\"\n" +
                "}"
    }
}